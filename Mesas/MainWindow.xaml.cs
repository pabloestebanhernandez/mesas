﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mesas
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string colorUsed = "#F44336";
        private string colorNoUsed = "#4CAF50";

        private Dictionary<string, mesa> mesas;


        public MainWindow()
        {
            InitializeComponent();

            mesas = new Dictionary<string, mesa>();
        }

        private void button_Copy8_Click(object sender, RoutedEventArgs e)
        {

            Button thisButton = ((Button)sender);

            string nameMesa = thisButton.Name;
            if (!mesas.ContainsKey(nameMesa))
            {



                FormUser f = new FormUser();
                f.Show();


                mesa m = new mesa() { nombre = f.NAME_MESA, nombreButton = nameMesa };
                mesas.Add(nameMesa, m);

                thisButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(colorUsed));
                thisButton.BorderBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom(colorUsed));
            }
            else
            {

                FormUser f = new FormUser(mesas[nameMesa]);
                f.Show();

                thisButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(colorNoUsed));
                thisButton.BorderBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom(colorNoUsed));
                mesas.Remove(nameMesa);
            }



       

        }
    }
}
