﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Mesas
{
    /// <summary>
    /// Lógica de interacción para FormUser.xaml
    /// </summary>
    public partial class FormUser : Window
    {

        public  string NAME_MESA { get; set; }

        public FormUser()
        {
            InitializeComponent();
        }

        public FormUser(mesa m)
        {
            InitializeComponent();
            NAME_MESA = m.nombre;
            textBlock.Text = m.nombre;
            textBox.IsEnabled = false;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            NAME_MESA = textBox.Text;
            this.Close();
        }
    }
}
